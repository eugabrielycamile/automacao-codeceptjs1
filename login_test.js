Feature('login');

Scenario('login com sucesso',  ({ I }) => {


    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.fillField('#user','gabrielycamile45@gmail.com')
    I.fillField('#password','123456')
    I.click('#btnLogin')
    I.waitForText('Login realizado',3)

}).tag('@sucesso')

Scenario('tentando logar digitando apenas o e-mail',  ({ I }) => {

    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.fillField('#user','gabrielycamile45@gmail.com')
    I.click('#btnLogin')
    I.waitForText('Senha inválida',3)
}).tag('@sóemail')

Scenario('tentando logar sem digitar o e-mail e senha',  ({ I }) => {

    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.click('#btnLogin')
    I.waitForText('E-mail inválido',3)

}).tag('@sememailesenha')

Scenario('tentando logar digitando apenas a senha',  ({ I }) => {


    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.fillField('#password','123456')
    I.click('#btnLogin')
    I.waitForText('E-mail inválido',3)

}).tag('sósenha')

